package nl.bioinf.marcelk.web2018.servlets;

import nl.bioinf.marcelk.web2018.config.WebConfig;
import org.thymeleaf.context.WebContext;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "BenchmarkServlet", urlPatterns = "/benchmark")
public class BenchmarkServlet extends HttpServlet {

    // Available OpenSSL algorithms to test
    private static final String[] algorithms = new String[] {"des", "des-ede3", "hmac", "md5", "aes",
            "camellia-192-cbc", "camellia-256-cbc", "rc4"};

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        process(request, response);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        process(request, response);
    }

    public void process(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        WebContext ctx = new WebContext(
                request,
                response,
                request.getServletContext(),
                request.getLocale());
        ctx.setVariable("options", algorithms);
        WebConfig.getTemplateEngine().process("openssl", ctx, response.getWriter());
    }
}
