package nl.bioinf.marcelk.web2018.servlets;

import nl.bioinf.marcelk.web2018.service.JobRunner;
import nl.bioinf.marcelk.web2018.service.SSLBenchmark;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * This servlet starts a benchmark run given a list of selected algorithms. Results are stored
 * based on the session ID. No view is returned after starting the benchmark run.
 */
@WebServlet(name = "RunBenchmarkServlet", urlPatterns = "/run")
public class RunBenchmarkServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String algorithms[] = request.getParameterValues("algorithms[]");

        HttpSession session = request.getSession(true);
        String sessionid = session.getId();

        // Build commands etc.
        SSLBenchmark benchmark = new SSLBenchmark(algorithms);
        JobRunner openSSLBenchmarker = new JobRunner(benchmark);
        // Setup job; output location and session ID
        String outputFolder = getServletContext().getInitParameter("output.storage");
        // Create output directory if it doesn't exist
        Files.createDirectories(Paths.get(outputFolder));

        // Start the job which will run in the background
        openSSLBenchmarker.startJob(outputFolder, sessionid);
    }
}
