package nl.bioinf.marcelk.web2018.servlets;

import com.google.gson.Gson;
import nl.bioinf.marcelk.web2018.model.Earthquake;
import nl.bioinf.marcelk.web2018.service.EarthquakeDataFetcher;

import javax.servlet.ServletContext;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@WebServlet(name = "EarthquakeLocationDataServlet", urlPatterns = "/locationdata")
public class EarthquakeLocationDataServlet extends HttpServlet {

    private List<Earthquake> events = null;

    @Override
    public void init() {
        EarthquakeDataFetcher fetcher = new EarthquakeDataFetcher();

        ServletContext context = getServletContext();
        InputStream data = context.getResourceAsStream("/data/earthquakes.txt");
        String result = new BufferedReader(new InputStreamReader(data, StandardCharsets.UTF_8))
                .lines().collect(Collectors.joining("\n"));

        this.events = fetcher.responseToEarthquakes(result);
    }

    /**
     * Retrieves data regarding recent earthquakes from the KNMI. Data is returned as JSON.
     * @param request
     * @param response
     * @throws IOException
     */
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String query = request.getParameter("query");

        Set<String> matches = this.events.stream()
                .map(e -> e.getLocation())
                .collect(Collectors.toSet());

        String json = new Gson().toJson(matches);

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(json);
    }
}
