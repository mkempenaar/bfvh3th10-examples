/**
 * Autocomplete functionality for searching and filtering earthquakes by a location.
 * Status: not implemented on website
 *  Data source: see EarthquakeLocationDataServlet
 *  JS source: see https://tarekraafat.github.io/autoComplete.js
 *  Required: css/autoComplete.css and js/autoComplete.min.js
 */
new autoComplete({
    data: {                              // Data src [Array, Function, Async] | (REQUIRED)
        src: async () => {
            // User search query
            const query = document.querySelector("#autoComplete").value;
            // Fetch External Data Source
            const source = await fetch(`/locationdata?query=${query}`);
            // Format data into JSON
            const data = await source.json();
            // Return Fetched data
            return data;
        },
        cache: true
    },
    placeHolder: "Enter a location",     // Place Holder text
    selector: "#autoComplete",           // Input field selector
    // threshold: 2,                     // Min. Chars length to start Engine
    debounce: 300,                       // Post duration for engine to start
    maxResults: 10,                      // Max. number of rendered results
    highlight: true,                     // Highlight matching results
    resultsList: {
        render: true,
        container: source => {
            source.setAttribute("id", "autoComplete_list");
        },
        destination: document.querySelector("#autoComplete"),
        position: "afterend",
        element: "ul"
    },
    resultItem: {
        content: (data, source) => {
            source.innerHTML = data.match;
        },
        element: "li"
    },
    onSelection: feedback => {
        console.log(feedback.selection.value);
    }
});

require([
    "esri/Map",
    "esri/widgets/Editor",
    "esri/views/MapView",
    "esri/request",
    "esri/layers/FeatureLayer",
    "esri/widgets/TimeSlider"
], function(Map, Editor, MapView, esriRequest, FeatureLayer, TimeSlider) {
    let layerView, previousFeatureObjIDs;

    let map = new Map({
        basemap: "topo-vector"
    });

    let view = new MapView({
        container: "viewDiv",
        map: map,
        center: [6.748, 53.31],
        zoom: 11
    });

    // Popup template, this will show a table listing the attributes
    let template = {
        title: "Earthquake in {location}",
        content: [
            {
                type: "fields",
                fieldInfos: [
                    {
                        fieldName: "date",
                        label: "Date"
                    },
                    {
                        fieldName: "magnitude",
                        label: "Magnitude"
                    },
                    {
                        fieldName: "depth",
                        label: "Depth in km"
                    }
                ]
            }
        ]
    };

    // Points are rendered differently depending on magnitude (size) and depth (color)
    let renderer = {
        type: "simple",
        symbol: {
            type: "simple-marker"
        },
        visualVariables: [{
            /* Variable to adjust point color based on depth. */
            type: "color",
            field: "depth",
            stops: [
                { value: 1, color: "rgba(247, 215, 148, 0.5)" },
                { value: 2, color: "rgba(241, 144, 102, 0.5)" },
                { value: 3, color: "rgba(196, 69, 105, 0.5)" }
            ]
        }, {
            /* Variable to adjust point size based on magnitude. */
            type: "size",
            field: "magnitude",
            stops: [
                { value: 1, size: 4 },
                { value: 2, size: 16 },
                { value: 2.5, size: 24},
                { value: 3, size: 32 },
                { value: 3.5, size: 40}
            ]
        }]
    };

    // Define the options for the request
    let options = {
        query: {
            limit: 30
        },
        responseType: "json"
    };

    let features = [];
    let objectID = 1;

    let dates = [];
    /**
     * Perform a GET request for earthquake data in JSON format.
     * Data is received as a list of features where each feature is an earthquake event. This data is
     * reformatted in the ArcGIS feature-format, combined into a FeatureLayer which is then added
     * to the existing map object for visualization.
     */
    esriRequest("/mapdatatest", options).then(function(response) {
        response.data.forEach(earthquake => {

            /* Extract data from each response element into a feature
            consisting of the geometry and any attributes */
            let geometry = {
                type: "point",
                x: parseFloat(earthquake['latLon'][1]),
                y: parseFloat(earthquake['latLon'][0])
            };

            // Parse the date to a JS date object
            let date = new Date(earthquake.date);
            dates.push({
                original: earthquake.date,
                converted: date,
                formatted: date.toLocaleString()
            });

            let attr = {
                ObjectID: objectID,
                location: earthquake.location,
                magnitude: earthquake.magnitude,
                date: date.getTime(),
                depth: earthquake.depth
            };

            features.push({
                geometry: geometry,
                attributes: attr
            });

            objectID++;
        });

        let earthquakeFeatureLayer = new FeatureLayer({
            source: features,
            objectIdField: "ObjectID",
            popupTemplate: template,
            renderer: renderer,
            timeInfo: {
                startField: "date", // name of the date field
                interval: { // specify time interval for
                    unit: "months",
                    value: 1
                }
            },
            fields: [
                {
                    name: "ObjectID",
                    type: "oid"
                }, {
                    name: "location",
                    type: "string"
                }, {
                    name: "magnitude",
                    type: "double"
                }, {
                    name: "date",
                    type: "date"
                }, {
                    name: "depth",
                    type: "double"
                }
            ]
        });

        // Add FeatureLayer to the map.
        map.add(earthquakeFeatureLayer);

        // create a new TimeSlider widget
        const timeSlider = new TimeSlider({
            container: "timeSlider",
            view: view,
            mode: "cumulative-from-start",
            playRate: 25,
            stops: {
                interval: {
                    value: 1,
                    unit: "days"
                }
            }
        });
        view.ui.add(timeSlider, "bottom-leading");

        /**
         * Click event for tracking clicks on earthquakes
         */
        view.on("click", function(event) {
            view.hitTest(event).then(function(response) {
                // check if a feature is returned from the earthquakeFeatureLayer
                // do something with the result graphic
                const graphic = response.results.filter(function(result) {
                    return result.graphic.layer === earthquakeFeatureLayer;
                })[0];

                console.log(graphic);
            });
        });

        view.whenLayerView(earthquakeFeatureLayer)
            .then(function(lv) {
                // The layerview for the layer
                layerView = lv;

                layerView.watch("updating", function(val){
                    if(!val){  // wait for the layer view to finish updating
                        layerView.queryFeatures().then(function(results){
                            let currentFeatures = results.features;
                            let currentFeatureObjIDs = currentFeatures.map(feature => {
                                return feature.attributes.ObjectID;
                            });
                            if (previousFeatureObjIDs !== undefined) {
                                // Compare current object IDs with previously shown
                                let previousFeatureObjIDsSet = new Set(previousFeatureObjIDs);
                                let difference = [...new Set([...currentFeatureObjIDs].filter(id => !previousFeatureObjIDsSet.has(id)))];
                                console.log("New Object IDs: ");
                                console.log(difference);
                            }
                            previousFeatureObjIDs = currentFeatureObjIDs;
                        });
                    }
                });

                const start = earthquakeFeatureLayer.timeInfo.fullTimeExtent.start;

                // Set start- and end-dates to the range provided by the data
                timeSlider.fullTimeExtent = {
                    start: start,
                    end: earthquakeFeatureLayer.timeInfo.fullTimeExtent.end
                };

                const end = new Date(start);
                // end of current time extent for time slider
                // showing earthquakes with one day interval
                end.setDate(end.getDate() + 14);

                // Values property is set so that timeslider
                // widget show the first day. We are setting
                // the thumbs positions.
                timeSlider.values = [start];
            });

        timeSlider.watch("timeExtent", function () {
            // update layer view filter to reflect current timeExtent
            layerView.effect = {
                filter: {
                    timeExtent: timeSlider.timeExtent,
                    geometry: view.extent
                },
                excludedEffect: "grayscale(20%) opacity(12%)"
            };
            //console.log("Querying features");
            //console.log(earthquakeFeatureLayer.queryFeatures());
        });


        view.when(function() {
            let editor = new Editor({
                view: view,
                allowedWorkflows: ["update"]
            });

            view.ui.add(editor, "top-right");

        })
    });
});